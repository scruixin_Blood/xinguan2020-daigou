/**
 * @param {Object} options
 * 网络请求 模块
 */
function network(options){
	return new Promise((resolve,reject)=>{
		let token = "";
		try {
		    token = uni.getStorageSync('token');
			if(options.data){
				options.data.token = token;
			}
		} catch (e) {
		   console.log(e+"错误信息");
		}
		
		if(!options.isHome){  //首页不需要显示  loading
			uni.showLoading({
				title: '正在加载',
				mask: false
			});
		}
		
		uniCloud.callFunction({
			name:options.url,
			data:options.data || {}
		}).then(res=>{
			uni.hideLoading()
			if(res.result.code !== 0){
				return Promise.reject(new Error(res.result.msg));
			}
			resolve(res.result);
		}).catch(err=>{
			uni.hideLoading()
			reject(err)
			uni.showToast({
				icon:"none",
				title: '稍后重试'+err.message
			});
		})
	})
}

export default network;
				